msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Qt-Contexts: true\n"
"X-Generator: Poedit 3.0.1\n"

#: kcharsets.cpp:156
msgctxt "KCharsets|@item Text character set"
msgid "Western European"
msgstr "დასავლეთ ევროპული"

#: kcharsets.cpp:162
msgctxt "KCharsets|@item Text character set"
msgid "Central European"
msgstr "ცენტრალურ ევროპული"

#: kcharsets.cpp:165
msgctxt "KCharsets|@item Text character set"
msgid "Baltic"
msgstr "ბალტიისპირეთის"

#: kcharsets.cpp:168
msgctxt "KCharsets|@item Text character set"
msgid "South-Eastern Europe"
msgstr "სამხრეთ დასავლური ევროპული"

#: kcharsets.cpp:171
msgctxt "KCharsets|@item Text character set"
msgid "Turkish"
msgstr "თურქული"

#: kcharsets.cpp:174
msgctxt "KCharsets|@item Text character set"
msgid "Cyrillic"
msgstr "კირილიცა"

#: kcharsets.cpp:180
msgctxt "KCharsets|@item Text character set"
msgid "Chinese Traditional"
msgstr "ჩინური ტრადიციული"

#: kcharsets.cpp:183
msgctxt "KCharsets|@item Text character set"
msgid "Chinese Simplified"
msgstr "ჩინური გაადვილებული"

#: kcharsets.cpp:187
msgctxt "KCharsets|@item Text character set"
msgid "Korean"
msgstr "კორეული"

#: kcharsets.cpp:190
msgctxt "KCharsets|@item Text character set"
msgid "Japanese"
msgstr "იაპონური"

#: kcharsets.cpp:194
msgctxt "KCharsets|@item Text character set"
msgid "Greek"
msgstr "ბერძნული"

#: kcharsets.cpp:197
msgctxt "KCharsets|@item Text character set"
msgid "Arabic"
msgstr "არაბული"

#: kcharsets.cpp:200
msgctxt "KCharsets|@item Text character set"
msgid "Hebrew"
msgstr "ივრითი"

#: kcharsets.cpp:205
msgctxt "KCharsets|@item Text character set"
msgid "Thai"
msgstr "ტაილანდური"

#: kcharsets.cpp:208
msgctxt "KCharsets|@item Text character set"
msgid "Unicode"
msgstr "უნიკოდი"

#: kcharsets.cpp:214
msgctxt "KCharsets|@item Text character set"
msgid "Northern Saami"
msgstr "ჩრდილო საამი"

#: kcharsets.cpp:216
msgctxt "KCharsets|@item Text character set"
msgid "Other"
msgstr "სხვა"

#: kcharsets.cpp:651
#, qt-format
msgctxt "KCharsets|@item %1 character set, %2 encoding"
msgid "%1 ( %2 )"
msgstr "%1 ( %2 )"

#: kcharsets.cpp:653
#, qt-format
msgctxt "KCharsets|@item"
msgid "Other encoding (%1)"
msgstr "სხვა კოდირება (%1)"

#: kcharsets.cpp:682
#, qt-format
msgctxt "KCharsets|@item Text encoding: %1 character set, %2 encoding"
msgid "%1 ( %2 )"
msgstr "%1 ( %2 )"

#: kemailaddress.cpp:503
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it contains more than one "
"@.\n"
"You will not create valid messages if you do not change your address."
msgstr ""
"ელფოსტის თქვენს მიერ შეყვანილი მისამართი არასწორია. ის ერთზე მეტ @-ს "
"შეიცავს.\n"
"თუ მისამართს არ შეცვლით, თქვენი ელფოსტა არასდროს გაიგზავნება."

#: kemailaddress.cpp:509
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it does not contain a @.\n"
"You will not create valid messages if you do not change your address."
msgstr ""
"ელფოსტის თქვენს მიერ შეყვანილი მისამართი არასწორია. ის @-ს არ შეიცავს.\n"
"თუ მისამართს არ შეცვლით, თქვენი ელფოსტა არასდროს გაიგზავნება."

#: kemailaddress.cpp:515
msgctxt "QObject|"
msgid "You have to enter something in the email address field."
msgstr "ელფოსტის მისამართის ველში რამე მაინც უნდა შეიყვანოთ."

#: kemailaddress.cpp:517
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it does not contain a "
"local part."
msgstr "ელფოსტის შეყვანილი მისამართი არასწორია. ის ლოკალურ ნაწილს არ შეიცავს."

#: kemailaddress.cpp:521
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it does not contain a "
"domain part."
msgstr "ელფოსტის შეყვანილი მისამართი არასწორია. ის დომენურ ნაწილს არ შეიცავს."

#: kemailaddress.cpp:525
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it contains unclosed "
"comments/brackets."
msgstr ""
"ელფოსტის თქვენს მიერ შეყვანილი მისამართის არასწორია. ის დაუხურავ კომენტარებს/"
"ბრჭყალებს შეიცავს."

#: kemailaddress.cpp:529
msgctxt "QObject|"
msgid "The email address you entered is valid."
msgstr "ელფოსტის თქვენს მიერ შეყვანილი მისამართი სწორია."

#: kemailaddress.cpp:531
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it contains an unclosed "
"angle bracket."
msgstr ""

#: kemailaddress.cpp:535
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it contains too many "
"closing angle brackets."
msgstr ""

#: kemailaddress.cpp:539
msgctxt "QObject|"
msgid ""
"The email address you have entered is not valid because it contains an "
"unexpected comma."
msgstr ""

#: kemailaddress.cpp:543
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it ended unexpectedly.\n"
"This probably means you have used an escaping type character like a '\\' as "
"the last character in your email address."
msgstr ""

#: kemailaddress.cpp:549
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it contains quoted text "
"which does not end."
msgstr ""

#: kemailaddress.cpp:553
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it does not seem to "
"contain an actual email address, i.e. something of the form joe@example.org."
msgstr ""

#: kemailaddress.cpp:558
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it contains an illegal "
"character."
msgstr ""

#: kemailaddress.cpp:562
msgctxt "QObject|"
msgid ""
"The email address you have entered is not valid because it contains an "
"invalid display name."
msgstr ""

#: kemailaddress.cpp:566
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid because it does not contain a "
"'.'.\n"
"You will not create valid messages if you do not change your address."
msgstr ""

#: kemailaddress.cpp:572
msgctxt "QObject|"
msgid "Unknown problem with email address"
msgstr "ელფოსტის მისამართის უცნობი პრობლემა"

#: kemailaddress.cpp:632
msgctxt "QObject|"
msgid ""
"The email address you entered is not valid.\n"
"It does not seem to contain an actual email address, i.e. something of the "
"form joe@example.org."
msgstr ""

#: kencodingprober.cpp:226 kencodingprober.cpp:267
msgctxt "KEncodingProber|@item Text character set"
msgid "Disabled"
msgstr "გამორთულია"

#: kencodingprober.cpp:228 kencodingprober.cpp:270
msgctxt "KEncodingProber|@item Text character set"
msgid "Universal"
msgstr "უნივერსალური"

#: kencodingprober.cpp:230 kencodingprober.cpp:312
msgctxt "KEncodingProber|@item Text character set"
msgid "Unicode"
msgstr "უნიკოდი"

#: kencodingprober.cpp:232 kencodingprober.cpp:282
msgctxt "KEncodingProber|@item Text character set"
msgid "Cyrillic"
msgstr "კირილიცა"

#: kencodingprober.cpp:234 kencodingprober.cpp:297
msgctxt "KEncodingProber|@item Text character set"
msgid "Western European"
msgstr "დასავლეთ ევროპული"

#: kencodingprober.cpp:236 kencodingprober.cpp:279
msgctxt "KEncodingProber|@item Text character set"
msgid "Central European"
msgstr "ცენტრალურ ევროპული"

#: kencodingprober.cpp:238 kencodingprober.cpp:285
msgctxt "KEncodingProber|@item Text character set"
msgid "Greek"
msgstr "ბერძნული"

#: kencodingprober.cpp:240 kencodingprober.cpp:288
msgctxt "KEncodingProber|@item Text character set"
msgid "Hebrew"
msgstr "ივრითი"

#: kencodingprober.cpp:242 kencodingprober.cpp:294
msgctxt "KEncodingProber|@item Text character set"
msgid "Turkish"
msgstr "თურქული"

#: kencodingprober.cpp:244 kencodingprober.cpp:291
msgctxt "KEncodingProber|@item Text character set"
msgid "Japanese"
msgstr "იაპონური"

#: kencodingprober.cpp:246 kencodingprober.cpp:276
msgctxt "KEncodingProber|@item Text character set"
msgid "Baltic"
msgstr "ბალტიისპირეთის"

#: kencodingprober.cpp:248 kencodingprober.cpp:300
msgctxt "KEncodingProber|@item Text character set"
msgid "Chinese Traditional"
msgstr "ჩინური ტრადიციული"

#: kencodingprober.cpp:250 kencodingprober.cpp:303
msgctxt "KEncodingProber|@item Text character set"
msgid "Chinese Simplified"
msgstr "ჩინური გაადვილებული"

#: kencodingprober.cpp:252 kencodingprober.cpp:306
msgctxt "KEncodingProber|@item Text character set"
msgid "Korean"
msgstr "კორეული"

#: kencodingprober.cpp:254 kencodingprober.cpp:309
msgctxt "KEncodingProber|@item Text character set"
msgid "Thai"
msgstr "ტაილანდური"

#: kencodingprober.cpp:256 kencodingprober.cpp:273
msgctxt "KEncodingProber|@item Text character set"
msgid "Arabic"
msgstr "არაბული"
